
export default defineNuxtPlugin(() => {
  const tokenSaver = useCookie("token");

  if(tokenSaver) {
    const store = $store().checkLogin;

        store.$patch({
        checkLogin: tokenSaver.value 
        });
  }
  
})