import snotify from 'vue3-snotify';
import 'vue3-snotify/style';
export default defineNuxtPlugin((nuxtApp) => {
    const { config } = nuxtApp.vueApp.use(snotify)
    return {
        provide: {
            snotify: config.globalProperties.$snotify
        }
    }
})