import _ from "lodash";
export default defineNuxtPlugin(() => {
  
  const config = useRuntimeConfig(),
    token = useCookie('token'),
    route = useRoute(),
    router = useRouter();
  const axiosSettings = $fetch.create({
  
    headers: {
      Accept: "application/json",
      "Cache-Control": "no-cache",
      Authorization: `Bearer ${token.value}`,
      shost: `paw.shop`,
      scart: ``
    },
    baseURL: config.public.API_BASE_URL,
    async onRequest({ request, options }) {
      const cartId = useCookie("cartId")
      const token = useCookie("token");
      options.headers["Authorization"] = `Bearer ${token.value}`;
      options.headers["scart"] = `${cartId.value}`

      // Log request
      // console.log("[fetch request]", request, options);
    },
    async onRequestError({ request, options, error }) {
    },
    async onResponse({ request, response, options }) {
    },
    async onResponseError({ request, response, options }) {
      if (response.status == 422) {
        let obj = {
          status: response.status,
          message: response._data.message,
          errors: {}
        }
        _.forEach(response._data.errors, function (value, key) {
          obj.errors[key] = value[0]
        });
        console.log(obj)
        response._data = obj
      }
      
      return response._data
    }
  })


  return {
    provide: {
      apiSettings: axiosSettings

    }
  }
})

