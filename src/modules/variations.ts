export default (fetch) => ({
    variations() {
      return fetch(`/variations`);
    },
    variationsGetProduct(payload) {
      return fetch(`variation/${payload}`)
    },
    variationGetProductsBySearch(payload) {
      return fetch(`/variations`, {params: payload})
    }
    
    
}) 