export default (fetch) => ({
    getCarts() { 
      return fetch(`/carts`);
    },
    addToCards(id,payload) {
        return fetch(`carts/${id}/add`, { body: payload, method: "post"})
    },
    syncCards(payload) {
        console.log(payload)
        return fetch(`/carts/sync`, { body: payload, method: "post"})
    }

})