import auth from "./auth";
import variations from "./variations";
import comments from "./comments";
import carts from "./carts"
import profile from "./profile"


export default () => {
    const { $apiSettings } = useNuxtApp();
    return {
        auth: auth($apiSettings),
        variations: variations($apiSettings),
        comments: comments($apiSettings),
        carts: carts($apiSettings),
        profile: profile($apiSettings)
    };
};
