export default (fetch) => ({
    login(payload) {
      return fetch(`/login`, { body: payload, method: "post" });
    },
    register(payload) {
      return fetch(`/register`, {body: payload, method: "post" })
    },
    logout(payload) {
      return fetch(`/logout`, {body: payload, method: "post"})
    }
  })
  